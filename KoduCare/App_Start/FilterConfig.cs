﻿using KoduCare.Filters;
using System.Web;
using System.Web.Mvc;

namespace KoduCare
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new InitializeLocationFilter());

        }
    }
}
