﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;
using System.Linq;
using System.IO;
using System;
using KoduCare.Code;
using System.Web.SessionState;
using System.Web;
using KoduCare.Controllers;
using KoduCare.Models;
using KoduCare.Repositories;
using KoduCare.ViewModels;
using Service;
using System.Text.RegularExpressions;
using System.Diagnostics;
using KoduCare.Domain;

namespace KoduCare.Filters
{
    public class InitializeLocationFilter : ActionFilterAttribute
	{
        private readonly Stopwatch _sw;

        public InitializeLocationFilter()
        {
            _sw = new Stopwatch();
        }

		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
            _sw.Restart();
			Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US"); 
            if (filterContext.Controller is IKoduCareController)
			{
                var siteLocation = (filterContext.Controller as IKoduCareController).SiteLocation;                
                var dbService = (filterContext.Controller as IKoduCareController).dbService;
                var db = new KoduCareDB();

                string ipaddr = filterContext.RequestContext.HttpContext.Request.ServerVariables["REMOTE_ADDR"];

                /*
                if (!Service.Cache.ApplicationIsSet(filterContext.HttpContext.Application, "Countries"))
                    Service.Cache.ApplicationSet(filterContext.HttpContext.Application, "Countries", dbService.Country.GetCountries());
                filterContext.Controller.ViewBag.Countries = Service.Cache.ApplicationGet(filterContext.HttpContext.Application, "Countries");
                 */
                //siteLocation.CurrentUser = dbService.User.GetCurrentUser();
                siteLocation.Controller = filterContext.RequestContext.RouteData.Values["controller"].ToString();
                siteLocation.Action = filterContext.RequestContext.RouteData.Values["action"].ToString();
                siteLocation.RouteData = filterContext.RequestContext.HttpContext.Request.QueryString;
                //siteLocation.CurrentUser = dbService.User.GetCurrentUser();
                //Return variable declaration
                var appPath = string.Empty;
                var context = HttpContext.Current;
                if (context != null)
                {
                    appPath = string.Format("{0}://{1}{2}",
                                            context.Request.Url.Scheme,
                                            context.Request.Url.Host,
                                            context.Request.Url.Port == 80
                                                ? string.Empty
                                                : ":" + context.Request.Url.Port
                        /*,context.Request.ApplicationPath*/);
                    if (!appPath.EndsWith("/")) appPath += "/"; siteLocation.Href_domain = appPath;

                    appPath = string.Format("{0}://{1}{2}{3}",
                                            context.Request.Url.Scheme,
                                            context.Request.Url.Host,
                                            context.Request.Url.Port == 80
                                                ? string.Empty
                                                : ":" + context.Request.Url.Port
                                            , context.Request.Url.AbsolutePath);
                    if (!appPath.EndsWith("/")) appPath += "/"; siteLocation.PageUrl = appPath;
                }
                siteLocation.Ip = filterContext.RequestContext.HttpContext.Request.ServerVariables["REMOTE_ADDR"];
                if (filterContext.RequestContext.HttpContext.Request.UrlReferrer!=null)
                {
                    siteLocation.Refferer = filterContext.RequestContext.HttpContext.Request.UrlReferrer.ToString();
                }                

                if (siteLocation.UserLocation == null)
                {
                    var url = filterContext.HttpContext.Request.Headers["Host"];
                    var subdomain = Regex.Replace(url, "((.*)(\\..*){2})|(.*)", "$2");
                    siteLocation.Subdomain = subdomain;

                    siteLocation.UserLocation = new UserLocation
                    {
                    };

                }
                siteLocation.UserLocation.Ip = ipaddr;
                filterContext.Controller.ViewBag.SiteLocation = siteLocation;                
            }
            
		}
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            _sw.Stop();
            var ms = _sw.Elapsed.TotalMilliseconds;
            if (filterContext.Result is ViewResult && !filterContext.RequestContext.HttpContext.Request.IsAjaxRequest())
            { //Make sure the request is a ViewResult, ie. a page
                ((ViewResult)filterContext.Result).TempData["ExecutionTime"] = ms; //Set the viewdata dictionary
            }
                        
        }
        private string GetControllerAndActionName(ActionDescriptor actionDescriptor)
        {
            return actionDescriptor.ControllerDescriptor.ControllerName + " - " + actionDescriptor.ActionName;
        }
	}
}