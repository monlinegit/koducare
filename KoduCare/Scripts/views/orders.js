﻿var userCurrLong;
var userCurrLat;
var map, myLatLng;
var keyTimer;
var pointMarker;

//On document ready
$(function () {
    //Bind event 'window ready' for gmaps
    google.maps.event.addDomListener(window, 'load', initialize);
    //Update map when window resize or screen orientation change
    $(window).resize(function () {
        if (map) {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(myLatLng);
        }
    });

    //Get coords when user type address (with delay)
    $("#UserAddress").on("keyup change", function () {
        if (keyTimer) { clearTimeout(keyTimer); keyTimer = null; }
        keyTimer = setTimeout(function () {
            keyTimer = null;
            var address = $("#UserAddress").val();
            codeAddress(address, function (latlng) {
                alertGeo(latlng);
            });       
        }, 1000);
    });
});

//Initialize and draw gmap
function initialize() {
    var mapOptions = {
        zoom: 15,
        disableDefaultUI: true
    }
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    myLatLng = new google.maps.LatLng(40.778651, -73.968975);
    map.setCenter(myLatLng);

    //Get current user location
    getLocation(alertGeo, geoError);
}

//Callback function, called when user location recieved
function alertGeo(pos) {
    var manual = false;//Is manual typing
    if (typeof pos.lat == "function") {
        userCurrLong = pos.lng();
        userCurrLat = pos.lat();
        manual = true;
    }
    else {
        userCurrLong = pos.coords.longitude;
        userCurrLat = pos.coords.latitude;
    }

    //Write position to form
    var latformatted = (userCurrLat + "").replace(/\./g, ',');
    var lngformatted = (userCurrLong + "").replace(/\./g, ',');

    if (!manual) {
        $("#UserLatAuto").val(latformatted);
        $("#UserLngAuto").val(lngformatted);
    }
    else {
        $("#UserLat").val(latformatted);
        $("#UserLng").val(lngformatted);
    }

    myLatLng = new google.maps.LatLng(userCurrLat, userCurrLong);
    //Get formatted address
    codeLatLng(myLatLng, function (address) {
        if (!manual) {
            $("#UserAddressAuto").val(address.formatted_address);
            if (!$("#UserAddress").val()) $("#UserAddress").val(address.formatted_address);
        }
        $("#UserAddressFormatted").val(address.formatted_address);
    });

    //Add marker to map
    var image = '/Content/img/marker2.png';
    if (!pointMarker) {
        var markerImage = new google.maps.MarkerImage(image,
                    null,
                    new google.maps.Point(0, 0),
                    new google.maps.Point(30, 39));

        pointMarker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: markerImage
        });
    }
    else {
        pointMarker.setPosition(myLatLng);
    }
    map.setCenter(myLatLng);
}

//Callback function, called when user location unknown
function geoError() {
    alert("We can`t get your address automatlically. Please, turn on GPS or enter your address into field.");
}