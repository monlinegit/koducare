﻿/*---------gps--------------------------------------------------------*/
//Get user location (with GPS or IP)
function getLocation(callback, callbackError, accuracy) {
    if (accuracy!==false) accuracy = true;
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(callback, function () {
            if (accuracy) {
                getLocation(callback, callbackError, false);
            }
            else callbackError();
        }, { maximumAge: 60000, timeout: 15000, enableHighAccuracy: accuracy });
    }
}

//Function for calc distance between points
function getGPSDistance(p1, p2) {
    var R = 6371; // Radius of the Earth in km
    var dLat = (p2.lat() - p1.lat()) * Math.PI / 180;
    var dLon = (p2.lng() - p1.lng()) * Math.PI / 180;
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(p1.lat() * Math.PI / 180) * Math.cos(p2.lat() * Math.PI / 180) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
};

//Get Address from GPS position
function codeLatLng(latlng, callback) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                //////////////////
                var arrAddress = results[0];
                callback(arrAddress);
                ///////////////////
            }
        }
    });
}

//Get GPS position from Address
function codeAddress(address, callback) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            callback(results[0].geometry.location);
        } else {
            
        }
    });
}
