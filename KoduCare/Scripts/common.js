/**
 * Created by Eugeniy on 10.03.2015.
 */
$(function() {
    $(window).on("resize", function () {
        $(".body-map").height($(window).height()-$("header").height());
    }).trigger("resize");
    $(".refresh").on("click", function(e) {
        e.preventDefault();
        $(this).prev().val("");
    });
    $('#hiw').on("click", function (e) {
        e.preventDefault();
        $.scrollTo(".how_it_works", 500);
    });
    $('#sun').on("click", function (e) {
        e.preventDefault();
        $.scrollTo(".get_start", 500);
    });
    $(".send_mailchimp").on("click", function () {
        if ($("#mce-success-response").css('display') == "block") $("#mce-success-response").css('display', 'none');
        $(this).addClass("load");
        setTimeout(function () {
            var interval = setInterval(function () {
                if ($("div.mce_inline_error").length > 0) {
                    $(".send_mailchimp").removeClass("load");
                    clearInterval(interval);
                }
                if ($("#mce-success-response").css('display') == "block") {
                    $(".send_mailchimp").removeClass("load");
                    clearInterval(interval);
                }
                console.log($("div.mce_inline_error").length + " " + $("#mce-success-response").css('display'));
            }, 300);
        }, 50);
    });
});
