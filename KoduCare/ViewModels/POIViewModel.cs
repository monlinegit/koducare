﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KoduCare.ViewModels
{
    public class POIViewModel
    {       
        public double? Lat { get; set; }
        public double? Lng { get; set; }
        public string Name { get; set; }
        public string TypeAlias { get; set; }
    }

    public class LatLng
    {       
        public double? Lat { get; set; }
        public double? Lng { get; set; }
    }


}