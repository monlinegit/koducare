﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KoduCare.ViewModels
{
	public class UserLocation
	{
        public double? UserLatitude { get; set; }
        public double? UserLongitude { get; set; }

        public string Ip { get; set; }
        /*
        public City City { get; set; }
        public Region Region { get; set; }
        public Country Country { get; set; }
         * */

	}
}