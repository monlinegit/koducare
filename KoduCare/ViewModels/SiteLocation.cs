﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using KoduCare.Domain;
using KoduCare.Models;

namespace KoduCare.ViewModels
{
    public class SiteLocation
    {
        public UserLocation UserLocation { get; set; }

        public string Subdomain { get; set; }
        public string Href_domain { get; set; }
        public string PageUrl { get; set; }
        public string CurrentPage { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public NameValueCollection RouteData { get; set; }
        public string ViewName { get; set; }
        public string Refferer { get; set; }

        public string Ip { get; set; }
        //public User CurrentUser { get; set; }
        public Dictionary<string, int> InfoParams { get; set; }

        public string currentCatAlias { get; set; }
    }
}