﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KoduCare.Code;

using KoduCare.ViewModels;
using KoduCare.Models;
using System.Data.Entity.SqlServer;
using System.Linq.Expressions;
using KoduCare.Domain;


namespace KoduCare.Repositories
{
    public class OrdersRepository: Repository
    {
        SiteLocation siteLocation;
        public OrdersRepository(SiteLocation siteLocation, KoduCareDB _db, DbService dbService)
            : base(_db, dbService)
        {

            this.siteLocation = siteLocation;
        }


        public int AddOrder(Order order)
        {
            var result = 0;
            try
            {
                db.Orders.Add(order);
                db.SaveChanges();
                result = order.Id;
            }
            catch { }
            return result;
        }



    }
}