﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using KoduCare.Domain;

namespace KoduCare.Repositories
{
    public abstract class Repository : IDisposable
    {
        protected KoduCareDB db;
        protected DbService dbService;

        public Repository(KoduCareDB _db, DbService dbService)
        {
            db = _db;
            this.dbService = dbService;
            var sqlConnection = db.Database.Connection as SqlConnection;
        }

        public void Dispose()
        {
            if (db != null) db.Dispose();
            GC.SuppressFinalize(this);
        }

    }
}