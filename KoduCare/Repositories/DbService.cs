﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using KoduCare.Models;
using KoduCare.ViewModels;
using KoduCare.Repositories;
using KoduCare.Domain;

namespace KoduCare.Repositories
{
    public class DbService : IDisposable
    {
        protected KoduCareDB db;
        public SiteLocation siteLocation;
        public string databaseName;
        protected OrdersRepository orders;
        
        public DbService(SiteLocation sitelocation, KoduCareDB db)
        {
            this.siteLocation = sitelocation;
            this.db = db;
            databaseName = db.Database.Connection.Database;
        }
        //Деструктор
        public void Dispose()
        {
            if (orders != null) orders.Dispose();

            db.Dispose();
            GC.SuppressFinalize(this);
        }
        public int ExecuteSqlCommand(string sql, int? timeout = null, params object[] parameters)
        {
            int? previousTimeout = null;
            if (timeout.HasValue)
            {
                //store previous timeout
                previousTimeout = ((IObjectContextAdapter)db).ObjectContext.CommandTimeout;
                ((IObjectContextAdapter)db).ObjectContext.CommandTimeout = timeout;
            }

            var result = db.Database.ExecuteSqlCommand(sql, parameters);

            if (timeout.HasValue)
            {
                //Set previous timeout back
                ((IObjectContextAdapter)db).ObjectContext.CommandTimeout = previousTimeout;
            }
            return result;
        }
        #region Свойства только для чтения
 
        public OrdersRepository Orders
        {
            get { if (orders == null) orders = new OrdersRepository(siteLocation, db, this); return orders; }
        }

 
        #endregion
    }
}