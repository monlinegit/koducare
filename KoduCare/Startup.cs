﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KoduCare.Startup))]
namespace KoduCare
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
