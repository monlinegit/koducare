﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KoduCare.Controllers
{
    public class HomeController : ApplicationController
    {
        //Home page
        public ActionResult Index()
        {
            return View();
        }

    }
}