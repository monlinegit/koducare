﻿using KoduCare.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KoduCare.Controllers
{
    public class OrderController : ApplicationController
    {
        // Page with map
        public ActionResult Index()
        {

            return View();
        }

        //Page with order result
        public ActionResult AddOrder(Order order)
        {
            order.Date = DateTime.Now;
            order.UserIP = siteLocation.Ip;
            var result = dbService.Orders.AddOrder(order);
            order.Id = result;

            var UserTexts = new Dictionary<string, string>();
            UserTexts["ABC"] = System.Configuration.ConfigurationManager.AppSettings["ABC"];
            ViewBag.UserTexts = UserTexts;

            return View(order);
        }
    }
}