﻿using ErrorLog;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using KoduCare.Models;
using KoduCare.Repositories;
using KoduCare.ViewModels;
using System.Web.Caching;
using KoduCare.Domain;

namespace KoduCare.Controllers
{
    public class ApplicationController : Controller, IKoduCareController
    {
        // GET: /Application/
        public SiteLocation siteLocation;
        public bool isMobile;
        public DbService dbService { get; set; }
        public SiteLocation SiteLocation { get { return siteLocation; } }
        public ErrorLog.Log Log;

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            siteLocation = new SiteLocation();
            var url = requestContext.HttpContext.Request.Url.Host;
            var subdomain = Regex.Replace(url, "((.*)(\\..*){2})|(.*)", "$2");
            siteLocation.Subdomain = subdomain;

            string DevideDBDomains = System.Configuration.ConfigurationManager.AppSettings["DevideDBDomains"];
            KoduCareDB db = null;

            db = new KoduCareDB();
            dbService = new DbService(siteLocation, db);

            var sqlConnection = db.Database.Connection as SqlConnection;
            Log = new ErrorLog.Log(sqlConnection);
            Log.SetTimingManagement(0,LogOutput.LONG_FUNCTIONS,LogDestinations.FILE);


            base.Initialize(requestContext);
        }
        public ApplicationController()
            : base()
        {
        }
       
        public static bool isMobileBrowser(HttpRequestBase Request)
        {
            //GETS THE CURRENT USER CONTEXT
            // HttpContext context = HttpContext.

            //FIRST TRY BUILT IN ASP.NT CHECK
            if (Request.Browser.IsMobileDevice)
            {
                return true;
            }
            //THEN TRY CHECKING FOR THE HTTP_X_WAP_PROFILE HEADER
            if (Request.ServerVariables["HTTP_X_WAP_PROFILE"] != null)
            {
                return true;
            }
            //THEN TRY CHECKING THAT HTTP_ACCEPT EXISTS AND CONTAINS WAP
            if (Request.ServerVariables["HTTP_ACCEPT"] != null &&
                Request.ServerVariables["HTTP_ACCEPT"].ToLower().Contains("wap"))
            {
                return true;
            }
            //AND FINALLY CHECK THE HTTP_USER_AGENT 
            //HEADER VARIABLE FOR ANY ONE OF THE FOLLOWING
            if (Request.ServerVariables["HTTP_USER_AGENT"] != null)
            {
                //Create a list of all mobile types
                string[] mobiles =
                    new[]
                {
                    "midp", "j2me", "avant", "docomo", 
                    "novarra", "palmos", "palmsource", 
                    "240x320", "opwv", "chtml",
                    "pda", "windows ce", "mmp/", 
                    "blackberry", "mib/", "symbian", 
                    "wireless", "nokia", "hand", "mobi",
                    "phone", "cdm", "up.b", "audio", 
                    "SIE-", "SEC-", "samsung", "HTC", 
                    "mot-", "mitsu", "sagem", "sony"
                    , "alcatel", "lg", "eric", "vx", 
                    "NEC", "philips", "mmm", "xx", 
                    "panasonic", "sharp", "wap", "sch",
                    "rover", "pocket", "benq", "java", 
                    "pt", "pg", "vox", "amoi", 
                    "bird", "compal", "kg", "voda",
                    "sany", "kdd", "dbt", "sendo", 
                    "sgh", "gradi", "jb", "dddi", 
                    "moto", "iphone"
                };
                string[] exceptions = new[]
                    {
                        "ipad"
                    };
                //Loop through each item in the list created above 
                //and check if the header contains that text
                foreach (string s in mobiles)
                {
                    foreach (string ex in exceptions)
                    {
                        if (Request.ServerVariables["HTTP_USER_AGENT"].
                                                            ToLower().Contains(s.ToLower()) && Request.ServerVariables["HTTP_USER_AGENT"].
                                                            ToLower().IndexOf(ex) == -1)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }
        public bool IsMobile(ref string Redirect)
        {

            //return false;
            var msite = false;
            var mobile = isMobileBrowser(Request); //mobile = true;

            string MobileAlways = System.Configuration.ConfigurationManager.AppSettings["MobileAlways"];
            if (MobileAlways == "1") { msite = true; return true; }

            var url = Request.Headers["Host"];
            var redirectUrl = "m." + url;
            var index = url.IndexOf(".");

            if (index != -1)
            {
                var subs = url.Split('.');
                if (subs[0] == "m")
                {
                    msite = true;
                    redirectUrl = string.Join(".", subs.Where(x => x != "m"));
                }
                else if (subs.Count() > 1) if (subs[0] == "www")
                    {
                        if (subs[1] == "m") msite = true;
                        subs[0] = "www.m";
                        redirectUrl = string.Join(".", subs);
                    }
            }

            if (msite == true)
            {
                if (mobile == false) { Redirect = redirectUrl; return false; }
                return true;
            }
          //  if (mobile == true) { Redirect = redirectUrl + "?fromFull=1"; return true; }
            return false;
        }
        protected void RegisterSiteVersion()
        {
            string RedirectUrl = "";
            bool mobile = IsMobile(ref RedirectUrl);
            if (HttpContext.Session != null)
            {
                HttpContext.Session["IsMobile"] = mobile;
                //HttpContext.Response.Redirect("http://www.google.ru?" + HttpContext.Session["FullVersion"]);
                if ((HttpContext.Session["FullVersion"] as int?) == 1)
                {
                    RedirectUrl = "";
                    mobile = false;
                }
            }
            if (!string.IsNullOrEmpty(RedirectUrl)) if (HttpContext.Request["FullVersion"] == "1")
                {
                    HttpContext.Session.Add("FullVersion", 1);
                    RedirectUrl = "";
                    mobile = false;
                }
            if (ControllerContext.HttpContext.Items["RedirectUrl"] == null) ControllerContext.HttpContext.Items.Add("RedirectUrl", RedirectUrl);
            else ControllerContext.HttpContext.Items["RedirectUrl"] = RedirectUrl;
            if (ControllerContext.HttpContext.Items["isMobile"] == null) ControllerContext.HttpContext.Items.Add("isMobile", mobile);
            else ControllerContext.HttpContext.Items["isMobile"] = mobile;

            this.isMobile = mobile;
        }
        protected new internal PartialViewResult PartialView()
        {

            return base.PartialView();
        }
        protected new internal PartialViewResult PartialView(string viewName)
        {
            RegisterSiteVersion();
            if (!this.isMobile) return base.PartialView(viewName);

            CheckForMobileEquivalentView(ref viewName, ControllerContext);
            return base.PartialView(viewName);
        }
        protected new internal PartialViewResult PartialView(string viewName, object model)
        {
            RegisterSiteVersion();
            if (!this.isMobile) return base.PartialView(viewName, model);

            CheckForMobileEquivalentView(ref viewName, ControllerContext);
            return base.PartialView(viewName, model);
        }
        protected new internal ViewResult View()
        {
            RegisterSiteVersion();
            if (!this.isMobile) return base.View();

            var viewName = ControllerContext.RouteData.GetRequiredString("action");
            CheckForMobileEquivalentView(ref viewName, ControllerContext);

            return base.View(viewName, (object)null);
        }
        protected new internal ViewResult View(object model)
        {
            RegisterSiteVersion();
            if (!this.isMobile) return base.View(model);

            var viewName = ControllerContext.RouteData.GetRequiredString("action");
            CheckForMobileEquivalentView(ref viewName, ControllerContext);

            return base.View(viewName, model);
        }
        protected new internal ViewResult View(string viewName)
        {
            RegisterSiteVersion();
            if (!this.isMobile) return base.View(viewName);

            CheckForMobileEquivalentView(ref viewName, ControllerContext);
            return base.View(viewName);
        }
        protected new internal ViewResult View(string viewName, object model)
        {
            RegisterSiteVersion();
            if (!this.isMobile) return base.View(viewName, model);

            CheckForMobileEquivalentView(ref viewName, ControllerContext);
            return base.View(viewName, model);
        }
        // Need this to prevent View(string, object) stealing calls to View(string, string)
        protected new internal ViewResult View(string viewName, string masterName)
        {
            RegisterSiteVersion();
            if (!this.isMobile) return base.View(viewName, masterName);
            return base.View(viewName, masterName);
        }
        private static void CheckForMobileEquivalentView(ref string viewName, ControllerContext ctx)
        {
            var mobileEquivalent = viewName;
            if (!viewName.Contains("~")) mobileEquivalent = "Mobile/" + viewName;

            viewName = mobileEquivalent;
            return;
        }

   
        protected override void OnActionExecuting(ActionExecutingContext context)
        {
            var Controller = context.RequestContext.RouteData.Values["controller"].ToString();
            var Action = context.RequestContext.RouteData.Values["action"].ToString();
            Log.RegisterFunctionBegin(Controller + "." + Action, context.RequestContext.HttpContext.Request.RawUrl);
        }

        protected override void OnActionExecuted(ActionExecutedContext context)
        {
            var Controller = context.RequestContext.RouteData.Values["controller"].ToString();
            var Action = context.RequestContext.RouteData.Values["action"].ToString();
            Log.RegisterFunctionEnd(Controller + "." + Action);
        }

    }
   

}
