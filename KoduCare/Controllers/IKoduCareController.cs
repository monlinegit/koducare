﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KoduCare.Repositories;
using KoduCare.ViewModels;

namespace KoduCare.Controllers
{
    public interface IKoduCareController
    {
        //
        // GET: /IKoduCare/

        DbService dbService { get; set; }
        SiteLocation SiteLocation { get; }

    }
}
