﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KoduCare.Domain
{
    [Table("Orders")]
    public class Order
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Display(Name = "Date of order")]
        public DateTime Date { get; set; }

        [Display(Name = "User address in form field (user can edit manualy)")]
        public string UserAddress { get; set; }

        [Display(Name = "User address, received automatically")]
        public string UserAddressAuto { get; set; }
        [Display(Name = "User address, formatted by google")]
        public string UserAddressFormatted { get; set; }
        [Display(Name = "User IP")]
        public string UserIP { get; set; }

        [Display(Name = "User latitude, from UserAddress")]
        public double? UserLat { get; set; }
        [Display(Name = "User longitude, from UserAddress")]
        public double? UserLng { get; set; }

        [Display(Name = "User latitude, received automatically")]
        public double? UserLatAuto { get; set; }
        [Display(Name = "User longitude, received automatically")]
        public double? UserLngAuto { get; set; }    

    }
}
