﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace KoduCare.Domain
{
    public class KoduCareDB : DbContext
    {
        public KoduCareDB()
            : base("KoduCare")
        {
        }

        public DbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            
            /*
            modelBuilder.Entity<Company>().HasMany(a => a.CompanyTypes).WithMany(b => b.Companies).Map(m =>
            {
                m.ToTable("CompanyCompanyTypes"); m.MapLeftKey("company_id"); m.MapRightKey("company_type_id");
            });

            modelBuilder.Entity<Company>().HasMany(a => a.Brands).WithMany(b => b.Companies).Map(m =>
            {
                m.ToTable("CompanyBrands"); m.MapLeftKey("CompanyId"); m.MapRightKey("BrandId");
            });

            modelBuilder.Entity<Good>()
            .HasOptional(f => f.GoodExtra) //Baz is dependent and gets a FK BarId
            .WithRequired(s => s.Good);//Bar is principal

            modelBuilder.Entity<User>().HasMany(a => a.Roles).WithMany(b => b.Users).Map(m =>
            {
                m.ToTable("webpages_UsersInRoles"); m.MapLeftKey("UserId"); m.MapRightKey("RoleId");
            });
*/
        }
    }
}