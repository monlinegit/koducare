﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;

namespace Service
{
	public class GeoIp
	{
		public static Nullable<IPInfo> getSingleIPInfo(string ip)
		{
			Nullable<IPInfo> ipInfo= null; 
            try
            {
                var longIp = IpToLong(ip);
                using (SqlConnection con = new SqlConnection("Data Source=.\\SQLEXPRESS;Initial Catalog=CommonServices;Integrated Security=SSPI"))
                using (SqlCommand cmd = new SqlCommand("SELECT [Id],[Region],[Lat],[Lng],[City] FROM [CommonServices].[dbo].[Artkiev_geo] with (nolock) WHERE (SELECT [Iidd] FROM [CommonServices].[dbo].[Artkiev_ip] with (nolock) WHERE " + longIp.ToString() + " >= [Artkiev_ip].ip1 AND " + longIp.ToString() + " <= [Artkiev_ip].ip2) = [Id]", con))
                {
                    cmd.CommandTimeout = 300;
                    con.Open();
                    using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                    using (DataSet ds = new DataSet())
                    {
                        adapter.Fill(ds);
                        var t = ds.Tables[0];
                        if (t.Rows.Count != 0)
                        {
                            ipInfo = new IPInfo
                            {
                                Ip = ip,
                                Region = t.Rows[0]["Region"].ToString(),
                                City = t.Rows[0]["City"].ToString(),
                                Latitude = double.Parse(t.Rows[0]["Lat"].ToString()),
                                Longitude = double.Parse(t.Rows[0]["Lng"].ToString())
                            };
                        }
                    }
                }
            }
            catch { }



			return ipInfo;
		}

        public static long IpToLong(string ip)
        {
            try
            {
                var a = ip.Split('.');
                long ret = long.Parse(a[0]) * 16777216 + long.Parse(a[1]) * 65536 + long.Parse(a[2]) * 256 + long.Parse(a[3]);
                return ret;
            }
            catch { return 0; }
        }

		private static string GetElementValue(XElement el)
		{
			return el == null ? null : el.Value.Trim();
		}

        public static Dictionary<string,int> GetRegions(string path)
        {
             Dictionary<string, int> categories = new Dictionary<string, int>();
             XDocument doc = XDocument.Load(path);
             var xcategories = doc.Element("regions").Elements();
             foreach (var xcategory in xcategories)
             {
                 string name = xcategory.Value;
                 string id = xcategory.Attribute("id").Value;
                 categories.Add(name, int.Parse(id));
             }
             return categories;
        }
	}

	public struct IPInfo
	{
		//
		public string City;
		public string Region;
		public double Latitude;
        public double Longitude;
		public string Ip;
	}

}