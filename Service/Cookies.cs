﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Net;
using System.Security.Cryptography;
using System.Web.Routing;
using System.Web.Mvc;
using System.Text;

namespace Service
{
    public class Cookies
    {

        public static bool Set(HttpContextBase context, string name, object value)
        {
            try
            {
                var valueString = value.ToString();
                var cookie = new HttpCookie(name, valueString);
                if (IsSet(context, name))
                {
                    cookie = context.Request.Cookies[name];
                    cookie.Value = valueString;
                    //DateTime expires = DateTime.Now.AddYears(1);
                    //_cookie.Expires = expires;
                    context.Response.Cookies.Set(cookie);
                }
                else context.Response.Cookies.Add(cookie);
                return true;
            }
            catch { }
            return false;
        }
        public static string Get(HttpContextBase context, string name)
        {
            string ret = null;   
            try
            {             
                if (IsSet(context, name, "Request"))
                    ret = context.Request.Cookies[name].Value.ToString();
                if (IsSet(context, name, "Response"))
                    ret = context.Response.Cookies[name].Value.ToString();
            }
            catch { }
            return ret;
        }
        public static bool IsSet(HttpContextBase context, string name, string type = "All")
        {
            var check1 = false;
            try
            {
                check1 = !string.IsNullOrEmpty(context.Request.Cookies[name].Value.ToString());
            }
            catch { }
            var check2 = false;
            try
            {
                check2 = !string.IsNullOrEmpty(context.Response.Cookies[name].Value.ToString());
            }
            catch { }
            return ((type == "All" || type == "Request") ? check1 : true) || ((type == "All" || type == "Response") ? check2 : true);
        }
        
    }
}