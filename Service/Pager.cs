﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Text;

namespace Service
{
	public class Pager
	{
		private readonly int pageSize;
		private readonly int currentPage;
		private readonly int totalItemCount;
        private readonly string jsFunction;

		public Pager(int pageSize, int currentPage, int totalItemCount,string jsFunction)
		{
			this.pageSize = pageSize;
			this.currentPage = currentPage;
			this.totalItemCount = totalItemCount;
            this.jsFunction = jsFunction;
		}

		public HtmlString RenderHtml()
		{
            
           
			var pageCount = (int)Math.Ceiling(totalItemCount / (double)pageSize);
            //const int nrOfPagesToDisplay = 10;

            var sb = new StringBuilder();

            // Previous
            sb.Append(currentPage > 1 ? GeneratePageLink("&larr;", currentPage - 1, "prev") : "<span class=\"disabled prev\">Предыдущая</span>");

            var start = 1;
            var end = pageCount;

            if (pageCount > pageSize)
            {
                var middle = (int)Math.Ceiling(pageSize / 2d) - 1;
                var below = (currentPage - middle);
                var above = (currentPage + middle);

                if (below < 4)
                {
                    above = pageSize;
                    below = 1;
                }
                else if (above > (pageCount - 4))
                {
                    above = pageCount;
                    below = (pageCount - pageSize + 1);
                }

                start = below;
                end = above;
            }

            if (start > 1)
            {
                sb.Append(GeneratePageLink("1", 1));
                if (start > 3)
                {
                    sb.Append(GeneratePageLink("2", 2));
                }
                if (start > 2)
                {
                    sb.Append("<span>...</span>");
                }
            }

            for (var i = start; i <= end; i++)
            {
                if (i == currentPage || (currentPage <= 0 && i == 0))
                {
                    sb.AppendFormat("<a class=\"gradient selected\" href=\"#\">{0}</a>", i);
                }
                else
                {
                    sb.Append(GeneratePageLink(i.ToString(), i));
                }
            }
            if (end < pageCount)
            {
                if (end < pageCount - 1)
                {
                    sb.Append("<span>...</span>");
                }
                if (pageCount - 2 > end)
                {
                    sb.Append(GeneratePageLink((pageCount - 1).ToString(), pageCount - 1));
                }
                sb.Append(GeneratePageLink(pageCount.ToString(), pageCount));
            }

            // Next
            sb.Append(currentPage < pageCount ? GeneratePageLink("&rarr;", (currentPage + 1), "next") : "<span class=\"disabled next\" href=\"#\">Следующая</span>");
            sb.AppendFormat("<div style=\"display:none;\" class=\"pagerData\" data-pages=\"{0}\" data-current=\"{1}\"></div>", pageCount, currentPage);
            return new HtmlString(sb.ToString());
		}

		private string GeneratePageLink(string linkText, int pageNumber, string classname=null)
		{
            return "<a href='#' onClick=\"" + jsFunction + "(" + pageNumber + ");return false;\" class=\"gradient " + classname + "\">" + linkText + "</a>";
		}
	}
}