﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Text;

namespace Service
{
	public class PagerMvc
	{
		private ViewContext viewContext;
		private readonly int pageSize;
		private readonly int currentPage;
		private readonly int totalItemCount;
		private readonly RouteValueDictionary linkWithoutPageValuesDictionary;
		private readonly AjaxOptions ajaxOptions;

		public PagerMvc(ViewContext viewContext, int pageSize, int currentPage, int totalItemCount, RouteValueDictionary valuesDictionary, AjaxOptions ajaxOptions)
		{
			this.viewContext = viewContext;
			this.pageSize = pageSize;
			this.currentPage = currentPage;
			this.totalItemCount = totalItemCount;
			this.linkWithoutPageValuesDictionary = valuesDictionary;
			this.ajaxOptions = ajaxOptions;

            if (pageSize != 40 && !valuesDictionary.ContainsKey("onpage")) valuesDictionary.Add("onpage", pageSize);
            if (valuesDictionary!=null && valuesDictionary["order"] != null && valuesDictionary["order"].ToString() == "date") valuesDictionary.Remove("order");
		}

		public HtmlString RenderHtml(bool isadmin=false)
		{
			var pageCount = (int)Math.Ceiling(totalItemCount / (double)pageSize);
			const int nrOfPagesToDisplay = 10;

			var sb = new StringBuilder();

			// Previous
            sb.Append(currentPage > 1 ? GeneratePageLink("ПРЕДЫДУЩАЯ", currentPage - 1, "prev") : "");
            sb.Append("<div class=\"pager_number\">");
			var start = 1;
			var end = pageCount;

			if (pageCount > nrOfPagesToDisplay)
			{
				var middle = (int)Math.Ceiling(nrOfPagesToDisplay / 2d) - 1;
				var below = (currentPage - middle);
				var above = (currentPage + middle);

				if (below < 4)
				{
					above = nrOfPagesToDisplay;
					below = 1;
				}
				else if (above > (pageCount - 4))
				{
					above = pageCount;
					below = (pageCount - nrOfPagesToDisplay + 1);
				}

				start = below;
				end = above;
			}

			if (start > 1)
			{
				sb.Append(GeneratePageLink("1", 1));
				if (start > 3)
				{
					sb.Append(GeneratePageLink("2", 2));
				}
				if (start > 2)
				{
                    sb.Append("<span>... </span>");
				}
			}

			for (var i = start; i <= end; i++)
			{
				if (i == currentPage || (currentPage <= 0 && i == 0))
				{
                   /* if (isadmin == false)
                    {
                        sb.AppendFormat("<a class=\"gradient selected\" href=\"#\">{0}</a>", i);
                    }
                    else
                    {
                        sb.AppendFormat("<span class=\"gradient selected\">{0}</span>", i);
                    }*/
                    sb.AppendFormat("<span class=\"gradient selected\">{0}</span>", i);
				}
				else
				{
					sb.Append(GeneratePageLink(i.ToString(), i));
				}
			}
			if (end < pageCount)
			{
				if (end < pageCount - 1)
				{
                    sb.Append("<span>...</span>");
				}
				if (pageCount - 2 > end)
				{
					sb.Append(GeneratePageLink((pageCount - 1).ToString(), pageCount - 1));
				}
				sb.Append(GeneratePageLink(pageCount.ToString(), pageCount));
			}
            sb.Append("</div>");
			// Next
            sb.Append(currentPage < pageCount ? GeneratePageLink("СЛЕДУЮЩАЯ", (currentPage + 1), "next") : "");

			return new HtmlString(sb.ToString());
		}

		private string GeneratePageLink(string linkText, int pageNumber, string classname=null)
		{
			var routeDataValues = viewContext.RequestContext.RouteData.Values;
			RouteValueDictionary pageLinkValueDictionary;
			// Avoid canonical errors when page count is equal to 1.
			if (pageNumber == 1)
			{
				pageLinkValueDictionary = new RouteValueDictionary(this.linkWithoutPageValuesDictionary);
				if (routeDataValues.ContainsKey("page"))
				{
					routeDataValues.Remove("page");
				}
			}
			else
			{
				pageLinkValueDictionary = new RouteValueDictionary(this.linkWithoutPageValuesDictionary) { { "page", pageNumber } };
			}

			// To be sure we get the right route, ensure the controller and action are specified.
			if (!pageLinkValueDictionary.ContainsKey("controller") && routeDataValues.ContainsKey("controller"))
			{
				pageLinkValueDictionary.Add("controller", routeDataValues["controller"]);
			}
			if (!pageLinkValueDictionary.ContainsKey("action") && routeDataValues.ContainsKey("action"))
			{
				pageLinkValueDictionary.Add("action", routeDataValues["action"]);
			}

			// 'Render' virtual path.
			var virtualPathForArea = RouteTable.Routes.GetVirtualPathForArea(viewContext.RequestContext, pageLinkValueDictionary);

			if (virtualPathForArea == null)
				return null;

            var stringBuilder = new StringBuilder("<a class=\"gradient" + (!string.IsNullOrEmpty(classname)?" "+classname:"") + "\" ");

			if (ajaxOptions != null)
				foreach (var ajaxOption in ajaxOptions.ToUnobtrusiveHtmlAttributes())
					stringBuilder.AppendFormat(" {0}=\"{1}\"", ajaxOption.Key, ajaxOption.Value);

            var path = virtualPathForArea.VirtualPath;
            var pos = path.IndexOf('?');
            if (pos != -1)
            {
                if (path.Substring(pos - 1, 1) != "/") path = path.Insert(pos, "/");
            }
            else
            {
                if (path.Substring(path.Length-1, 1) != "/") path += "/";
            }

            stringBuilder.AppendFormat(" href=\"{0}\">{1}</a>", path, linkText);		
			return stringBuilder.ToString();
		}
	}
}