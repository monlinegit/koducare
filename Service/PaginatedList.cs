﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Service
{
    public class PaginatedList<T> : List<T>
    {
        public int PageIndex { get; private set; }
        public int PageSize { get; private set; }
        public int TotalCount { get; private set; }
        public int TotalPages { get; private set; }

        public PaginatedList(IQueryable<T> source, int pageIndex, int pageSize, bool justadd = false, int SourceCount = 0)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
            if (source == null) return;            
            if (SourceCount!=0) TotalCount = SourceCount;
			else TotalCount = source.Count();
            TotalPages = (int)Math.Ceiling(TotalCount / (double)PageSize);

            if (justadd) this.AddRange(source);
            else this.AddRange(source.Skip((PageIndex - 1) * PageSize).Take(PageSize));
        }

        public bool HasPreviousPage
        {
            get
            {
                return (PageIndex > 0);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (PageIndex + 1 < TotalPages);
            }
        }
    }
}