﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Net;
using System.Security.Cryptography;
using System.Web.Routing;
using System.Web.Mvc;
using System.Text;

namespace Service
{
    public class Text
    {
            public static string TruncateAtWord(string input, int length)
            {
                if (input == null || input.Length < length)
                    return input;
                int iNextSpace = input.LastIndexOf(" ", length);
                var ret = string.Format("{0}...", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
                return ret;
            }

            public static string DeleteHtmlTags(string input)
            {
                string content = Regex.Replace(input, @"</?.+?>", "");
                return content;
            }

            public static string Translit(string text)
            {
                string[] strArray1 = new string[33]
      {"а", "б", "в", "г", "д", "е", "ё", "ж", "з", "и", "й",
        "к",
        "л",
        "м",
        "н",
        "о",
        "п",
        "р",
        "с",
        "т",
        "у",
        "ф",
        "х",
        "ц",
        "ч",
        "ш",
        "щ",
        "ъ",
        "ь",
        "ы",
        "э",
        "ю",
        "я"
      };
                string[] strArray2 = new string[33]
      {
        "a",
        "b",
        "v",
        "g",
        "d",
        "e",
        "yo",
        "zh",
        "z",
        "i",
        "y",
        "k",
        "l",
        "m",
        "n",
        "o",
        "p",
        "r",
        "s",
        "t",
        "u",
        "f",
        "h",
        "ts",
        "ch",
        "sh",
        "sht",
        "a",
        "y",
        "y",
        "e",
        "yu",
        "ya"
      };
                for (int index = 0; index < Enumerable.Count<string>((IEnumerable<string>)strArray1); ++index)
                {
                    text = text.Replace(strArray1[index], strArray2[index]);
                    text = text.Replace(strArray1[index].ToUpper(), strArray2[index].ToUpper());
                }
                text = text.Replace(" ", "_");
                text = Regex.Replace(text, "[^\\w_-]+", "");
                return string.IsNullOrEmpty(text) ? null : text.Replace('-', '_');
            }
        
            public static string BigLetter(string s)
            {
                if (s == null) return "";
                if (s.Length < 2) return s.ToUpper();
                return s.Substring(0, 1).ToUpper() + s.Substring(1);
            }
            
            public static string LinkifyText(String sContent)
            {
                Regex regx = new Regex("(?<!(?:href='|<a[^>]*>))(http|https|ftp|ftps)://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*)?", RegexOptions.IgnoreCase);
                MatchCollection matches = regx.Matches(sContent);
                for (int i = matches.Count - 1; i >= 0; i--)
                {
                    var pos = matches[i].Index; var len = 20;
                    if (pos < 20) len = pos;
                    if (sContent.Substring(pos - len, len).IndexOf("<a") != -1) continue;
                    string newURL = "<a href='" + matches[i].Value + "'>" + matches[i].Value + "</a>";
                    sContent = sContent.Remove(matches[i].Index, matches[i].Length).Insert(matches[i].Index, newURL);
                }
                return sContent;

            }

        

    }
}