﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KoduCare.Code
{
    public class OAuthClient : DotNetOpenAuth.AspNet.IAuthenticationClient
    {
        private string clientName;

        public OAuthClient(string clientName)
        {
            this.clientName=clientName;
        }

        public string ProviderName
        {
            get { return this.clientName; }
        }

        public void RequestAuthentication(HttpContextBase context, Uri returnUrl)
        {
            
        }

        public DotNetOpenAuth.AspNet.AuthenticationResult VerifyAuthentication(HttpContextBase context)
        {
            throw new NotImplementedException();
        }
    }
}