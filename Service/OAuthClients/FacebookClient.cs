﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.Helpers;
using System.Collections;
using System.Net;

namespace KoduCare.Code
{

    public class FacebookClient : DotNetOpenAuth.AspNet.IAuthenticationClient
    {

        private const string AuthorizationEndpoint = "https://www.facebook.com/dialog/oauth";
        private const string TokenEndpoint = "https://graph.facebook.com/oauth/access_token";
        private readonly string _appId;
        private readonly string _appSecret;
        public string returnUrl;
       
        public FacebookClient(string appId, string appSecret)
        {
            this._appId = appId;
            this._appSecret = appSecret;
        }
 
        public string GetServiceLoginUrl(Uri returnUrl)
        {
            this.returnUrl = returnUrl.ToString();
            return 
                AuthorizationEndpoint
                + "?client_id=" + this._appId
                + "&redirect_uri=" + HttpUtility.UrlEncode(returnUrl.ToString())
             //   + "&display=popup"
             ;
        }
        
        public string QueryAccessToken(string returnUrl, string authorizationCode)
        {
            WebClient client = new WebClient();
            string content = client.DownloadString(
                TokenEndpoint
                + "?client_id=" + this._appId
                + "&client_secret=" + this._appSecret
                + "&redirect_uri=" + HttpUtility.UrlEncode(this.returnUrl.ToString())
                + "&code=" + authorizationCode
            );

            NameValueCollection nameValueCollection = HttpUtility.ParseQueryString(content);
            if (nameValueCollection != null)
            {
                string result = nameValueCollection["access_token"];
                return result;
            }
            return null;
        }

        public IDictionary GetUserData(string accessToken)
        {
            WebClient client = new WebClient();
            string content = client.DownloadString(
                "https://graph.facebook.com/me?access_token=" + accessToken
            );
            dynamic data = Json.Decode(content);
            return new Dictionary<string,object> {
                {
                    "id",
                    data.id
                }, 
                {
                    "name",
                    data.name
                },
                {
                    "first_name",
                    data.first_name
                },
                {
                    "last_name",
                    data.last_name
                },
                {
                    "photo",
                    "https://graph.facebook.com/" + data.id + "/picture"
                }
            };
        }
        
        public string ProviderName
        {
            get { return "facebook"; }
        }

        public void RequestAuthentication(HttpContextBase context, Uri returnUrl)
        {
            context.Response.Redirect(GetServiceLoginUrl(returnUrl));
        }

        public DotNetOpenAuth.AspNet.AuthenticationResult VerifyAuthentication(HttpContextBase context)
        {


            return new DotNetOpenAuth.AspNet.AuthenticationResult(true);
            //throw new NotImplementedException();
        }


    }

}