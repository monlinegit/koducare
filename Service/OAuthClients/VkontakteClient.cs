﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.Helpers;
using System.Collections;
using System.Net;
using System.Text;
using System.Web.Helpers;
namespace KoduCare.Code
{

    public class VkontakteClient : DotNetOpenAuth.AspNet.IAuthenticationClient
    {

        private const string AuthorizationEndpoint = "https://oauth.vk.com/authorize";
        private const string TokenEndpoint = "https://oauth.vk.com/access_token";
        private readonly string _appId;
        private readonly string _appSecret;
        public string returnUrl;
        public string user_id;
       
        public VkontakteClient(string appId, string appSecret)
        {
            this._appId = appId;
            this._appSecret = appSecret;
        }
 
        public string GetServiceLoginUrl(Uri returnUrl)
        {
            this.returnUrl = returnUrl.ToString();
            return 
                AuthorizationEndpoint
                + "?client_id=" + this._appId
                + "&redirect_uri=" + HttpUtility.UrlEncode(returnUrl.ToString())
                + "&response_type=code"
             //   + "&display=popup"
             ;
        }
        
        public string QueryAccessToken( string authorizationCode)
        {
            WebClient client = new WebClient();
            string content = client.DownloadString(
                TokenEndpoint
                + "?client_id=" + this._appId
                + "&client_secret=" + this._appSecret
                + "&redirect_uri=" + HttpUtility.UrlEncode(returnUrl.ToString())
                + "&code=" + authorizationCode
            );

            dynamic nameValueCollection = Json.Decode(content);
            if (nameValueCollection != null)
            {
                string result = nameValueCollection["access_token"];
                this.user_id = nameValueCollection["user_id"].ToString();
                return result;
            }
            return null;
        }

        public IDictionary GetUserData(string accessToken)
        {
            WebClient client = new WebClient(); client.Encoding = Encoding.UTF8;
            string content = client.DownloadString(
                "https://api.vk.com/method/users.get?uids=" + this.user_id + "&access_token=" + accessToken + "&fields=uid,first_name,last_name,nickname,sex,bdate,city,country,timezone,photo,photo_medium,photo_big,photo_rec,connections"
            );
            dynamic data = Json.Decode(content);
            return new Dictionary<string,object> {
                {
                    "id",
                    data.response[0].uid
                }, 
                {
                    "name",
                    data.response[0].first_name +" "+ data.response[0].last_name
                },
                {
                    "first_name",
                    data.response[0].first_name
                },
                {
                    "last_name",
                    data.response[0].last_name
                },
                {
                    "photo",
                    data.response[0].photo_big
                }
            };
        }
        
        public string ProviderName
        {
            get { return "vk"; }
        }

        public void RequestAuthentication(HttpContextBase context, Uri returnUrl)
        {
            context.Response.Redirect(GetServiceLoginUrl(returnUrl));
        }

        public DotNetOpenAuth.AspNet.AuthenticationResult VerifyAuthentication(HttpContextBase context)
        {


            return new DotNetOpenAuth.AspNet.AuthenticationResult(true);
            //throw new NotImplementedException();
        }


    }

}