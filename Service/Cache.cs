﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Net;
using System.Security.Cryptography;
using System.Web.Routing;
using System.Web.Mvc;
using System.Text;


namespace Service
{
    public class Cache
    {
        public static bool ApplicationIsSet(HttpApplicationStateBase context,string key)
        {
            return context.AllKeys.Contains(key);
        }

        public static object ApplicationGet(HttpApplicationStateBase context, string key)
        {
            if (context.AllKeys.Contains(key))
            {
                return context[key];
            }
            return null;
        }

        public static bool ApplicationSet(HttpApplicationStateBase context, string key, object value)
        {
            try
            {
                context.Lock();
                context.Add(key, value);
                context.UnLock();
            }
            catch
            {
                return false;
            }
            return true;
        }      

    }
}