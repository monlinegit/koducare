﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Net;
using System.Security.Cryptography;
using System.Web.Routing;
using System.Web.Mvc;
using System.Text;

namespace Service
{
    public class Sessions
    {

        public static bool Set(HttpContextBase context, string name, object value)
        {
            try
            {
                var valueString = value.ToString();
                context.Session[name] = value;
              
                return true;
            }
            catch { }
            return false;
        }
        public static string Get(HttpContextBase context, string name)
        {
            string ret = null;   
            try
            {             
                ret = context.Session[name].ToString();
            }
            catch { }
            return ret;
        }
        public static void Remove(HttpContextBase context, string name)
        {
            
            try
            {
                context.Session.Remove(name);
            }
            catch { }
         
        }
        public static bool IsSet(HttpContextBase context, string name, string type = "All")
        {
            var check1 = false;
            try
            {
                check1 = !string.IsNullOrEmpty(context.Request.Cookies[name].Value.ToString());
            }
            catch { }
            var check2 = false;
            try
            {
                check2 = !string.IsNullOrEmpty(context.Response.Cookies[name].Value.ToString());
            }
            catch { }
            return ((type == "All" || type == "Request") ? check1 : true) || ((type == "All" || type == "Response") ? check2 : true);
        }
        
    }
}