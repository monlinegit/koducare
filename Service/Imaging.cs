﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Dynamic;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Net;
using System.Security.Cryptography;
using System.Web.Routing;
using System.Web.Mvc;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace Service
{
    public class Imaging
    {
        public static Bitmap AddWatermark(Bitmap image, Bitmap watermark)
        {
            if (image != null && watermark != null)
            {
                int w = watermark.Width;
                int h = watermark.Height;
                using (Graphics graphics = Graphics.FromImage(image))
                {
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    var posx = (image.Width - w) / 2.0;
                    var posy = (image.Height - h) / 2.0;
                    graphics.DrawImage(watermark, (float)posx, (float)posy, w, h);
                }
            }
            return image;
        }

        public static string GetAliasForPhoto(string path, string filename, int id, string alias)
        {
            var newFilename = id.ToString() + "-" + alias;
            return GetAliasForPhoto(path, filename, newFilename);
        }
        public static string GetAliasForPhoto(string path, string filename, string newFilename)
        {
            var tmp = filename.Split('.'); var extent = tmp.ElementAt(tmp.Count() - 1);
            filename = newFilename;
            var random = new Random(); var new_filename = "";
            var num = 0;
            do
            {
                new_filename = filename + DateTime.Now.Second + (num > 0 ? num.ToString() : "") + "." + extent;
                num = random.Next(100);
            } while (File.Exists(path + new_filename));
            return new_filename;
        }
       
        public static string SaveBitmap(Bitmap bitmap, string path, string filename, Imaging.Resizer? resizer, out Bitmap result, bool getalias = true)
        {
            result = null;
            try
            {
                if (resizer != null)
                {
                    ImageResizer.ResizeSettings sett = new ImageResizer.ResizeSettings();
                    //Ресайзим до нужной высоты всегда
                    if (bitmap.Height < resizer.Value.height)
                    {
                        sett.Height = resizer.Value.height;
                        sett.Scale = ImageResizer.ScaleMode.Both;
                        sett.BackgroundColor = Color.White;
                        bitmap = ImageResizer.ImageBuilder.Current.Build(bitmap, sett);
                    }

                    //Обрезаем или оставляем белые поля
                    if (bitmap.Height > bitmap.Width) sett.Mode = resizer.Value.fitmodeVertical.HasValue ? resizer.Value.fitmodeVertical.Value : ImageResizer.FitMode.Pad;
                    else sett.Mode = resizer.Value.fitmode.HasValue ? resizer.Value.fitmode.Value : ImageResizer.FitMode.Crop;

                    sett.Scale = ImageResizer.ScaleMode.Both;                    
                    sett.BackgroundColor = Color.White;
                    sett.Width = resizer.Value.width;
                    sett.Height = resizer.Value.height;

                    bitmap = ImageResizer.ImageBuilder.Current.Build(bitmap, sett);
                    if (resizer.Value.watermark!=null)
                    {
                        bitmap = Imaging.AddWatermark(bitmap, resizer.Value.watermark);
                    }
                }

                if(getalias)filename = Util.GetAliasForFile(path, filename, true);
                result = new Bitmap(bitmap);
                ////////////////////
                ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
                ImageCodecInfo jgpEncoder=null;
                foreach (ImageCodecInfo codec in codecs)
                {
                    if (codec.FormatID == ImageFormat.Jpeg.Guid)
                    {
                        jgpEncoder =codec;
                    }
                }
                 

                
                System.Drawing.Imaging.Encoder myEncoder =
                    System.Drawing.Imaging.Encoder.Quality;

              
                EncoderParameters myEncoderParameters = new EncoderParameters(1);

                EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 90L);
                myEncoderParameters.Param[0] = myEncoderParameter;
               
                //////////////////
                bitmap.Save(path + filename, jgpEncoder, myEncoderParameters);                
            }
            catch { filename = ""; }

            return filename;
        }
        public static string SaveBitmap(Bitmap bitmap, string path, string filename, Imaging.Resizer? resizer, bool getalias = true)
        {
            Bitmap tmp = null;
            return SaveBitmap(bitmap, path, filename, resizer,out tmp,getalias);
        }

        public ImageCodecInfo GetEncoder()
        {

            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == ImageFormat.Jpeg.Guid)
                {
                    return codec;
                }
            }
            return null;
        }
        public static string UploadPhoto(HttpPostedFileBase stream, string path, string newFilename, out Bitmap original,bool getalias=true)
        {
            string filename = "";
            original = null;

            try
            {
                var hpf = stream;
                if (hpf != null)
                {
                    filename = hpf.FileName;
                    if (newFilename != null)
                    {
                        var file_name_array = filename.Split('.');
                        filename = newFilename + "." + file_name_array[file_name_array.Length-1];
                    }

                    if(getalias)filename = Util.GetAliasForFile(path, filename, true);

                    Bitmap bitmap = new Bitmap(hpf.InputStream);
                    bitmap.Save(path + filename);
                    original = new Bitmap(bitmap);
                }
                else return null;
            }
            catch { return null; }
            return filename;
        }




        public struct Resizer
        {
            public int width { get; set; }
            public int height { get; set; }
            public Bitmap watermark { get; set; }
            public ImageResizer.FitMode? fitmode { get; set; }
            public ImageResizer.FitMode? fitmodeVertical { get; set; }

        }
        public static string UploadPhoto(string http_path, string path, string newFilename, out Bitmap original, bool getalias = true)
        {
            string filename = "";
            original = null;

            try
            {
                // var hpf = stream;
                if (!string.IsNullOrEmpty(http_path))
                {
                    var last_slash = http_path.LastIndexOf("/");
                    filename = http_path.Substring(last_slash + 1);
                    if (newFilename != null)
                    {
                        var file_name_array = filename.Split('.');
                        filename = newFilename + "." + file_name_array[file_name_array.Length - 1];
                    }

                    if (getalias) filename = Util.GetAliasForFile(path, filename, true);
                    var request = WebRequest.Create(http_path);
                    var response = request.GetResponse();
                    var stream = response.GetResponseStream();

                    Bitmap bitmap = Bitmap.FromStream(stream) as Bitmap;
                    bitmap.Save(path + filename);
                    original = new Bitmap(bitmap);
                }
                else return null;
            }
            catch { return null; }
            return filename;
        }
    }
}