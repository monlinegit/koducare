﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Net;
using System.Security.Cryptography;
using System.Web.Routing;
using System.Web.Mvc;
using System.Text;
using System.Xml;
using RazorEngine;



namespace Service
{
    public class Util
    {

        public static string ViewToString(string template)
        {
            template = System.IO.File.ReadAllText(template);
            string result = Razor.Parse(template);
            return result;
        }

        public static string[] getYandexChordsByAddress(string address)
        {
            address = address.Trim().Replace(' ', '+');
            var xmlReader = XmlReader.Create("http://geocode-maps.yandex.ru/1.x/?geocode="+address);
            var res = "";
            while (xmlReader.Read())
            {
                if (xmlReader.Name == "pos")
                {
                    res = xmlReader.ReadElementContentAsString();
                }
            }

            return res.Split(' ');


        }

        public static string GetAliasForFile(string path, string filename, bool isextension)
        {
            string alias = String.Empty;
            bool isfile = true;
            int count_of_file = 0;
            string file_name = filename.Substring(0, filename.LastIndexOf("."));
            string exc = filename.Substring(filename.LastIndexOf(".") + 1);
            //string[] file_name_array = filename.Split('.');//разделяем имя файла на имя и расширение
            alias = Text.Translit(file_name);
            string real_alias = alias;
            while (isfile == true)
            {
                string tmp = alias + "." + exc;
                if (!File.Exists(path + tmp))
                {
                    //count_of_file = 0;
                    isfile = false;
                }
                else
                {
                    count_of_file++;
                    alias = file_name + "_" + count_of_file.ToString();

                }

            }
            if (isextension == false)
            {
                if (count_of_file != 0)
                {
                    alias = real_alias + "_" + count_of_file.ToString();
                }
                else
                {
                    alias = real_alias;
                }
                return alias;
            }
            else
            {
                string alias_text = real_alias + "." + exc;
                if (count_of_file != 0)
                {
                    alias_text = real_alias + "_" + count_of_file.ToString() + "." + exc;
                }

                return alias_text;
            }

        }
        public static string GetAliasForDirectory(string path, string filename)
        {
            string alias = String.Empty;
            bool isfile = true;
            int count_of_file = 0;
            
                alias = Text.Translit(filename);
            string real_alias = alias;
            while (isfile == true)
            {
                string tmp = alias;
                if (!Directory.Exists(path + tmp))
                {
                    isfile = false;
                }
                else
                {
                    count_of_file++;
                    alias = filename + "_" + count_of_file.ToString();

                }

            }
            return alias;
        }

        public static string GetAlias(string str, int length = -1)
        {
            if (string.IsNullOrEmpty(str)) return "";
            var ret = Text.Translit(str);
            if (length != -1)
            {
                if (ret.Length > length) ret = ret.Substring(0, length);
            }
            return ret;
        }
        
        public static bool isEmailValid(string email)
        {              
         string expr = "[.\\-_a-z0-9]+@([a-z0-9][\\-a-z0-9]+\\.)+[a-z]{2,6}";  
      
               Match isMatch =   
                 Regex.Match(email, expr, RegexOptions.IgnoreCase);  
      
               return isMatch.Success; 
        }
        public static bool isOnlyDigits(string str)
        {
            string expr = "^\\d+$";  
      
               Match isMatch =   
                 Regex.Match(str, expr, RegexOptions.IgnoreCase);  
      
               return isMatch.Success; 
        }
        public static string StripTags(string str)
        {
            if (str == null) return str;
            str = str.Replace("<br>", "\n").Replace("<br/>", "\n").Replace("<br />", "\n");
            return Regex.Replace(str, "<[^>]*>", "");
        }

        public static string GeneratePassword(int length = 8)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            char[] chars = new char[length];
            Random rd = new Random();

            for (int i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }
            return new string(chars);
        }

        public static double AsFloat(string s,int sign=6)
        {
            float result = 0;
            if (string.IsNullOrEmpty(s)) return 0;
            try
            {
                result= float.Parse(s, CultureInfo.GetCultureInfo("uk"));
            }
            catch (FormatException)
            {
                result= float.Parse(s, CultureInfo.InvariantCulture);
            }
            return Math.Round(result, sign);
        }
        public static string UpFirstLetter(string s)
        {
            try
            {
                return s.Substring(0, 1).ToUpper() + s.Substring(1).ToLower();
            }
            catch { }
            return s;
        }

        public static void ClearDirectory(string path)
        {
            DirectoryInfo dir = new DirectoryInfo(path);
            var files=dir.GetFileSystemInfos();
            foreach (var file in files)
            {
                if (file.Attributes.HasFlag(FileAttributes.Directory))
                {
                    ClearDirectory(path + file.Name+"/");
                }
                file.Delete();
            }            
        }
        public static string GetHtmlPage(string strURL)
        {

            String strResult;
            WebResponse objResponse;
            WebRequest objRequest = HttpWebRequest.Create(strURL);
            objResponse = objRequest.GetResponse();
            using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
            {
                strResult = sr.ReadToEnd();
                sr.Close();
            }
            return strResult;
        }

        public static bool IsEmpty(string str)
        {
            if (str == null) return true;
            if (str == string.Empty) return true;
            return false;
        }

        public static string ReturnOnlyDigits(string str)
        {
            if (string.IsNullOrEmpty(str)) return "";
            return Regex.Replace(str,"[^\\d]+","");
        }

        public static string SHA1(string str)
        {
            string encode = str;
            ASCIIEncoding UE = new ASCIIEncoding();
            byte[] HashValue, MessageBytes = UE.GetBytes(encode);
            SHA1Managed SHhash = new SHA1Managed();
            string strHex = "";

            HashValue = SHhash.ComputeHash(MessageBytes);
            foreach (byte b in HashValue)
            {
                strHex += String.Format("{0:x2}", b);
            }
            return strHex;
        }  

        public static string Curl(string uri, string parameters)
        {
            // parameters: name1=value1&name2=value2	
            WebRequest webRequest = WebRequest.Create(uri);
            //string ProxyString = 
            //   System.Configuration.ConfigurationManager.AppSettings
            //   [GetConfigKey("proxy")];
            //webRequest.Proxy = new WebProxy (ProxyString, true);
            //Commenting out above required change to App.Config
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(parameters);
            Stream os = null;
            try
            { // send the Post
                webRequest.ContentLength = bytes.Length;   //Count bytes to send
                os = webRequest.GetRequestStream();
                os.Write(bytes, 0, bytes.Length);         //Send it
            }
            catch
            {
                return null;
            }
            finally
            {
                if (os != null)
                {
                    os.Close();
                }
            }

            try
            { // get the response
                WebResponse webResponse = webRequest.GetResponse();
                if (webResponse == null)
                { return null; }
                StreamReader sr = new StreamReader(webResponse.GetResponseStream());
                return sr.ReadToEnd().Trim();
            }
            catch 
            {
                return null;
            }
        }

        public static string CutString(string str, int length)
        {
            if (str.Length <= length) return str;
            str = str.Substring(0, length) + "...";
            return str;
        }


        public static string LZW_decompress(string s){
            if (string.IsNullOrEmpty(s)) return "";

            try
            {
                List<int> compressed = new List<int>();
                foreach (var c in s)
                {
                    compressed.Add((int)c);
                }

                // build the dictionary
                Dictionary<int, string> dictionary = new Dictionary<int, string>();
                for (int i = 0; i < 256; i++)
                    dictionary.Add(i, ((char)i).ToString());

                string w = dictionary[compressed[0]];
                compressed.RemoveAt(0);
                StringBuilder decompressed = new StringBuilder(w);

                foreach (int k in compressed)
                {
                    string entry = null;
                    if (dictionary.ContainsKey(k))
                        entry = dictionary[k];
                    else if (k == dictionary.Count)
                        entry = w + w[0];

                    decompressed.Append(entry);

                    // new sequence; add it to the dictionary
                    dictionary.Add(dictionary.Count, w + entry[0]);

                    w = entry;
                }

                return decompressed.ToString();
            }
            catch { return ""; }
        }

        public static string DecodeFrom64(string encodedData)
        {
            if (string.IsNullOrEmpty(encodedData)) return "";
            string text = "";
            try
            {                
                byte[] buffer = Convert.FromBase64String(encodedData);
                text = System.Text.Encoding.UTF8.GetString(buffer);
            }
            catch { }
            return text;
        }
        
    }
}